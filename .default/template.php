<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false)) {
		return;
	}
}

if ($arResult['NavPageCount'] <= 1) {
	return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
?>

<div class="pagination">
	<ul>
		<? if ($arResult["NavPageNomer"] > 1): ?>
			<? if ($arResult["NavPageNomer"] > 2): ?>
				<li>
					<a href="<?= $arResult["sUrlPath"]?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"]?>=<?= ($arResult["NavPageNomer"]-1) ?>" class="prev-page"><?= GetMessage("nav_prev") ?></a>
				</li>
			<? else: ?>
				<li>
					<a href="<?= $arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="prev-page"><?= GetMessage("nav_prev") ?></a>
				</li>
			<? endif ?>
		<? else: ?>
			<li>
				<a class="prev-page"><?= GetMessage("nav_prev") ?></a>
			</li>
		<? endif ?>
		<? while($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>
			<? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
				<li class="active">
					<a><span><?= $arResult["nStartPage"] ?></span></a>
				</li>
			<? elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
				<li>
					<a href="<?= $arResult["sUrlPath"] ?><?=$strNavQueryStringFull?>">
						<span><?= $arResult["nStartPage"] ?></span>
					</a>
				</li>
			<? else: ?>
				<li>
					<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>">
						<span><?= $arResult["nStartPage"] ?></span>
					</a>
				</li>
			<? endif ?>
			<? $arResult["nStartPage"]++ ?>
		<? endwhile ?>
		<? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
			<li>
				<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"]+1) ?>" class="next-page"><?= GetMessage("nav_next") ?></a>
			</li>
		<? else: ?>
			<li>
				<a class="next-page"><?= GetMessage("nav_next") ?></a>
			</li>
		<? endif ?>
	</ul>
</div>